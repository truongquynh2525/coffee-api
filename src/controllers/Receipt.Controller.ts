import { Request, Response } from "express";
import { get } from "lodash";
import { FindCategory } from "../services/Category.Service";
import {
  DeleteReceipt,
  FindReceipt,
  FindAllReceipt,
  UpdateReceipt,
} from "../services/Receipt.Service";
import { FindUser, GetUserID } from "../services/User.Service";

export const CheckReceiptID = async (res: Response, receiptID: string) => {
  const receipt = await FindReceipt({ _id: receiptID });
  if (!receipt) {
    return res.status(404).json({ message: "RECEIPT IS NOT FOUND" });
  }
  return receipt;
};

export const CreateReceiptHandler = async (req: Request, res: Response) => {
  const categoryID = req.body.category;
  const category = await FindCategory({ _id: categoryID });
  if (!category) {
    return res.status(404).json({ message: "CATEGORY IS NOT FOUND" });
  }

  const userID = await GetUserID(req);
  const receipt = await FindReceipt({ category: categoryID });

  const quantityStock =
    parseInt(receipt.quantityStock) + parseInt(req.body.quantityStock);
  const price =
    (parseInt(receipt.price.toString()) + parseInt(req.body.price)) / 2;
  const result = await UpdateReceipt(
    { _id: receipt._id },
    { quantityStock: quantityStock, user: userID, price: price },
    { new: true }
  );
  return res.status(200).json({ result });
};

export const UpdateReceiptHandler = async (req: Request, res: Response) => {
  const receiptID = get(req, "params.id");
  await CheckReceiptID(res, receiptID);

  const userID = await GetUserID(req);
  const result = await UpdateReceipt(
    { _id: receiptID },
    { ...req.body, user: userID },
    { new: true }
  );

  return res.status(200).json({ result });
};

export const DeleteReceiptHandler = async (req: Request, res: Response) => {
  const receiptID = get(req, "params.id");
  await CheckReceiptID(res, receiptID);
  const receipt = await DeleteReceipt({ _id: receiptID });
  if (receipt.quantityStock > 0) {
    return res.status(405).json({ message: "CAN'T REMOVE THIS RECEIPT" });
  }
  return res.status(200).json({ message: "DELETE SUCCESSFUL" });
};

export const GetListReceiptHandler = async (req: Request, res: Response) => {
  const list: any = await FindAllReceipt({});
  let result: any = [];
  for (let i = 0; i < list.length; i++) {
    const categoryID = list[i].category;
    const category = await FindCategory({ _id: categoryID });
    const user = await FindUser({ _id: list[i].user });
    const name = user?.lastName + " " + user?.firstName;
    result[i] = { ...list[i]._doc, category: category?.name, user: name };
  }
  return res.status(200).json({ result });
};

export const GetReceiptByIDHandler = async (req: Request, res: Response) => {
  const receiptID = get(req, "params.id");
  const receipt: any = await CheckReceiptID(res, receiptID);
  const user = await FindUser({ _id: receipt.user });
  const name = user?.lastName + " " + user?.firstName;
  const result = { ...receipt._doc, user: name };
  return res.status(200).json({ result });
};
