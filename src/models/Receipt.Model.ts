import mongoose from "mongoose";
import { UserDocument } from "./User.Model";
import { CategoryDocument } from "./Category.Model";

export interface ReceiptDocument extends mongoose.Document {
  quantityStock: number;
  price: number;
  dateGet: Date;
  category: CategoryDocument["_id"];
  user: UserDocument["_id"];
}

const ReceiptSchema = new mongoose.Schema({
  quantityStock: {
    type: Number,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  dateGet: {
    type: Date,
    default: Date.now(),
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category",
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const Receipt = mongoose.model("Receipt", ReceiptSchema);

export default Receipt;
